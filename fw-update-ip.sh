#!/bin/bash
# Usage:
# fw-update-ip.sh <ip>

#########################

# https://stackoverflow.com/a/13778973
validate_ip() {
  # Set up local variables
  local ip=${1:-1.2.3.4}
  local IFS=.; local -a a=($ip)
  # Start with a regex format test
  [[ $ip =~ ^[0-9]+(\.[0-9]+){3}$ ]] || return 0
  # Test values of quads
  local quad
  for quad in {0..3}; do
    [[ "${a[$quad]}" -gt 255 ]] && return 0
  done
  return 1
}

#########################

if [ "$EUID" -ne 0 ]
  then echo "Please run this script as root"
  exit 1
fi

if [ -z "$1" ]
  then echo "IP address must be specified"
  exit 1
fi

if validate_ip "$1"
  then echo "IP address is not valid"
  exit 1
fi

echo "RULES BEFORE EDITING:"
ufw status numbered

declare -a rules=("3306" "22/tcp" "21/tcp")

for rule in "${rules[@]}"
do
  echo "Rule '$rule':"
  num=$(ufw status numbered | grep " $rule" | awk -F"[][]" '{print $2}' | tr --delete "[:blank:]")
  if [ -n "$num" ]
    then ufw --force delete "$(ufw status numbered | grep "$rule" | awk -F"[][]" '{print $2}' | tr --delete "[:blank:]")"
  fi
  rule=$(echo "$rule" | sed -e 's/\// proto /g')
  ufw allow from "$1" to any port $rule
  printf "\n"
done

printf "RULES AFTER EDITING:\n"
ufw status numbered
echo "Done"