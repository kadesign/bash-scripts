#!/bin/bash
# Usage:
# piserver-le-getcert -d <domain> -e <email> [-r]

# Options:
# -d     Domain
# -e     Email for letsencrypt request
# -r     Enable subdomain redirect

# Todos:
# 1. Add checking domain A records
# 2. Add fallback to no-ssl if there was an error obtaining a certificate

#########################

NGINX_DIR="/etc/nginx"

#########################

check_domain () {
  DOMAIN_CHECK="$(echo -e "${1}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//' | grep -P '(?=^.{4,253}$)(^(?:[a-zA-Z0-9](?:(?:[a-zA-Z0-9\-]){0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,}$)')"
}

check_email () {
  EMAIL_CHECK="$(echo -e "${1}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//' | grep -P '^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-z]{2,}')"
}

#########################

if [ "$EUID" -ne 0 ]
  then echo "Please run this script as root"
  exit 1
fi

SUBDOMAIN_REDIRECT=false

while getopts "d:e:r" OPTION; do
  case $OPTION in
  d)
    check_domain "$OPTARG"
    OPTARG=$DOMAIN_CHECK
    if [[ -z "$OPTARG" ]]
    then
      echo "Argument -d must contain valid domain name"
      exit 1
    else
      DOMAIN=$OPTARG
    fi
    ;;
  e)
    check_email "$OPTARG"
    OPTARG=$EMAIL_CHECK
    if [[ -z "$OPTARG" ]]
    then
      echo "Argument -e must contain valid e-mail address."
      exit 1
    else
      EMAIL=$OPTARG
    fi
    ;;
  r)
    SUBDOMAIN_REDIRECT=true
    ;;
  *)
    echo "Usage: piserver-le-getcert -d <domain> -e <email>"
    exit 1
    ;;
  esac
done

# Comment out SSL related directives in configuration
sed -i -r 's/(listen .*443)/\1;#/g; s/(ssl_(certificate|certificate_key|trusted_certificate) )/#;#\1/g' "$NGINX_DIR/sites-available/$DOMAIN.conf"

# Reload nginx
echo "Reloading nginx..."
sudo nginx -t && sudo systemctl reload nginx

# Obtain certificate
echo "Obtaining certificate from Let's Encrypt..."
if [ "$SUBDOMAIN_REDIRECT" = true ] ; then
  certbot certonly --webroot -d "$DOMAIN" -d "www.$DOMAIN" --email "$EMAIL" -w /var/www/_letsencrypt -n --agree-tos --force-renewal
else
  certbot certonly --webroot -d "$DOMAIN" --email "$EMAIL" -w /var/www/_letsencrypt -n --agree-tos --force-renewal
fi

# Uncomment SSL related directives in configuration
sed -i -r 's/#?;#//g' "$NGINX_DIR/sites-available/$DOMAIN.conf"

# Reload nginx
echo "Reloading nginx..."
sudo nginx -t && sudo systemctl reload nginx
