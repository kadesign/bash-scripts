#!/bin/bash
# Usage:
# piserver-create-static -d <domain> -e <email> [-r]

# Options:
# -d     Domain
# -e     Email for Let's Encrypt request
# -r     Enable subdomain redirect

#########################

NGINX_DIR="/etc/nginx"

#########################

check_domain () {
  DOMAIN_CHECK="$(echo -e "${1}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//' | grep -P '(?=^.{4,253}$)(^(?:[a-zA-Z0-9](?:(?:[a-zA-Z0-9\-]){0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,}$)')"
}

check_email () {
  EMAIL_CHECK="$(echo -e "${1}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//' | grep -P '^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-z]{2,}')"
}

#########################

SUBDOMAIN_REDIRECT=false

if [ "$EUID" -ne 0 ]
  then echo "Please run this script as root"
  exit 1
fi

while getopts "d:e:r" OPTION; do
  case $OPTION in
  d)
    check_domain "$OPTARG"
    OPTARG=$DOMAIN_CHECK
    if [[ -z "$OPTARG" ]]
    then
      echo "Argument -d must contain valid domain name"
      exit 1
    else
      DOMAIN=$OPTARG
    fi
    ;;
  e)
    check_email "$OPTARG"
    OPTARG=$EMAIL_CHECK
    if [[ -z "$OPTARG" ]]
    then
      echo "Argument -e must contain valid e-mail address."
      exit 1
    else
      EMAIL=$OPTARG
    fi
    ;;
  r)
    SUBDOMAIN_REDIRECT=true
    ;;
  *)
    echo "Usage: piserver-create-static -d <domain> -e <email> [-r]"
    exit 1
    ;;
  esac
done

while [[ -z "$DOMAIN" ]]; do
  read -r -p "Domain: " DOMAIN

  check_domain "$DOMAIN"
  DOMAIN=$DOMAIN_CHECK

  if [[ -z "$DOMAIN" ]]
  then
    echo "Please type in valid domain name!"
  fi
done

while [[ -z "$EMAIL" ]]; do
  read -r -p "Email: " EMAIL

  check_email "$EMAIL"
  EMAIL=$EMAIL_CHECK

  if [[ -z "$EMAIL" ]]
  then
    echo "Please type in valid e-mail address!"
  fi
done

echo "Options:"
echo "Domain = $DOMAIN"
echo "Email for Let's Encrypt = $EMAIL"
echo "Enable subdomain redirect = $SUBDOMAIN_REDIRECT"

echo "Creating nginx configuration for domain..."

# Copy config template and insert domain name
domain=$DOMAIN envsubst '$domain' < "$NGINX_DIR/sites-available/static-template.conf" > "$NGINX_DIR/sites-available/$DOMAIN.conf"

# Create root directory and apply rights/ownership
mkdir -p "/var/www/$DOMAIN/public"
chown -R www-data:www-data "/var/www/$DOMAIN"
chmod -R 0755 "/var/www/$DOMAIN"

# Create soft link to the configuration
ln -s "$NGINX_DIR/sites-available/$DOMAIN.conf" "$NGINX_DIR/sites-enabled/$DOMAIN.conf"

# Uncomment redirect section if applicable
if [ "$SUBDOMAIN_REDIRECT" = true ] ; then
  sed -i -r 's/(# nr #)//g' "$NGINX_DIR/sites-available/$DOMAIN.conf"
fi

# Obtain certificate from Let's Encrypt
bash ./piserver-le-getcert.sh -d "$DOMAIN" -e "$EMAIL"

echo "Done."

exit 0;
