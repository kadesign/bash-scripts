#!/bin/bash
# Usage:
# piserver-create-node-express -d <domain> -p <port> -e <email> [-r]

# Options:
# -d     Domain
# -p     Port
# -e     Email for Let's Encrypt request
# -r     Enable subdomain redirect

#########################

NGINX_DIR="/etc/nginx"

#########################

check_domain () {
  DOMAIN_CHECK="$(echo -e "${1}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//' | grep -P '(?=^.{4,253}$)(^(?:[a-zA-Z0-9](?:(?:[a-zA-Z0-9\-]){0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,}$)')"
}

check_email () {
  EMAIL_CHECK="$(echo -e "${1}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//' | grep -P '^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-z]{2,}')"
}

check_port () {
  PORT_CHECK="$(echo -e "${1}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//' | grep -P '^[0-9]{1,5}')"
}

#########################

SUBDOMAIN_REDIRECT=false

if [ "$EUID" -ne 0 ]
  then echo "Please run this script as root"
  exit 1
fi

while getopts "d:p:e:r" OPTION; do
  case $OPTION in
  d)
    check_domain "$OPTARG"
    OPTARG=$DOMAIN_CHECK
    if [[ -z "$OPTARG" ]]
    then
      echo "Argument -d must contain valid domain name"
      exit 1
    else
      DOMAIN=$OPTARG
    fi
    ;;
  p)
    check_port "$OPTARG"
    OPTARG=$PORT_CHECK
    if [[ -z "$OPTARG" ]]
    then
      echo "Argument -p must contain valid port
      name"
      exit 1
    else
      PORT=$OPTARG
    fi
    ;;
  e)
    check_email "$OPTARG"
    OPTARG=$EMAIL_CHECK
    if [[ -z "$OPTARG" ]]
    then
      echo "Argument -e must contain valid e-mail address."
      exit 1
    else
      EMAIL=$OPTARG
    fi
    ;;
  r)
    SUBDOMAIN_REDIRECT=true
    ;;
  *)
    echo "Usage: piserver-create-node-express -d <domain> -p <port> -e <email> [-r]"
    exit 1
    ;;
  esac
done

while [[ -z "$DOMAIN" ]]; do
  read -r -p "Domain: " DOMAIN

  check_domain "$DOMAIN"
  DOMAIN=$DOMAIN_CHECK

  if [[ -z "$DOMAIN" ]]
  then
    echo "Please type in valid domain name!"
  fi
done

while [[ -z "$PORT" ]]; do
  read -r -p "Port: " PORT

  check_domain "$PORT"
  PORT=$PORT_CHECK

  if [[ -z "$PORT" ]]
  then
    echo "Please type in valid port!"
  fi
done

while [[ -z "$EMAIL" ]]; do
  read -r -p "Email: " EMAIL

  check_email "$EMAIL"
  EMAIL=$EMAIL_CHECK

  if [[ -z "$EMAIL" ]]
  then
    echo "Please type in valid e-mail address!"
  fi
done

echo "Options:"
echo "Domain = $DOMAIN"
echo "Port = $PORT"
echo "Email for Let's Encrypt = $EMAIL"
echo "Enable subdomain redirect = $SUBDOMAIN_REDIRECT"

echo "Creating nginx configuration for domain..."

# Copy config template and insert domain name
shdomain=$DOMAIN shport=$PORT envsubst '$shdomain $shport' < "$NGINX_DIR/sites-available/node-express-template.conf" > "$NGINX_DIR/sites-available/$DOMAIN.conf"

# Create soft link to the configuration
ln -s "$NGINX_DIR/sites-available/$DOMAIN.conf" "$NGINX_DIR/sites-enabled/$DOMAIN.conf"

# Uncomment redirect section if applicable and obtain certificate from Let's Encrypt
if [ "$SUBDOMAIN_REDIRECT" = true ] ; then
  sed -i -r 's/(# nr #)//g' "$NGINX_DIR/sites-available/$DOMAIN.conf"
  bash ./piserver-le-getcert.sh -d "$DOMAIN" -e "$EMAIL" -r
else
  bash ./piserver-le-getcert.sh -d "$DOMAIN" -e "$EMAIL"
fi

echo "Done."

exit 0;
