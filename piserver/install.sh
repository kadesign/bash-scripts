#!/bin/bash
# Usage:
# install

#########################

NGINX_DIR="/etc/nginx"

#########################

if [ "$EUID" -ne 0 ]
  then echo "Please run this script as root"
  exit 1
fi

# Copy config templates
echo "Copying nginx configuration..."
cp -r ./nginx-config/snippets/. "$NGINX_DIR/snippets"
cp -r ./nginx-config/sites-available/. "$NGINX_DIR/sites-available/"
mv "$NGINX_DIR/nginx.conf" "$NGINX_DIR/nginx.conf.bak"
cp ./nginx-config/nginx.conf "$NGINX_DIR/nginx.conf"
rm "$NGINX_DIR/sites-enabled/default"

# Reload nginx
echo "Reloading nginx..."
nginx -t && systemctl reload nginx

# Prepare Let's Encrypt
openssl dhparam -out /etc/nginx/dhparam.pem 2048
mkdir -p /var/www/_letsencrypt
chown www-data /var/www/_letsencrypt

echo -e '#!/bin/bash\nnginx -t && systemctl reload nginx' | tee /etc/letsencrypt/renewal-hooks/post/nginx-reload.sh
chmod a+x /etc/letsencrypt/renewal-hooks/post/nginx-reload.sh

echo "Done."

exit 0;
